#XL Binding Framework. V0.01

This is a purely VBA implementation of a binding framework. The project is inspired by angularJS.

Objective
-
Bind a sheet to a Model Class as simply as possible.

Assuming sheet1 has 2 named ranges `sheet1!name` & `sheet1!output`. Once `bindapp.module` is called, the sheet and the model will have 2 way binding.
Save and Load functions are available by default to the workbooks folder as 



    'modStartup
    public sub auto_open
        Call bindapp.Module("BaseUser", Sheet1, New User)
    end sub
    public sub btnSave()
        bindapp.Modules("BaseUser").Save
    end sub
	public sub btnLoad()
        bindapp.Modules("BaseUser").Load
    end sub
	
    'clsUser
    Option Explicit
    Implements IModel

    Private m_sname As String
    Private m_soutput As String

    Public Property Get name() As String
        name = m_sname
    End Property

    Public Property Let name(ByVal sname As String)
        m_sname = sname
    End Property

    Public Property Get output() As String
        output = "{ name " & name & ", time " & DateTime.Now & " }"
    End Property


-------------

