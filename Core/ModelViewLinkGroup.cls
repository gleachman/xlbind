VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "ModelViewLinkGroup"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
Implements IModelViewLink

Public Sub link(ByVal sFieldName As String, ByRef nmRange As name, ByRef oModel As Object, ByRef oModule As Object)

    Dim lcolHash As String
    
    Set oModule.dicGetters(sFieldName) = nmRange
    Set oModule.dicSetters(sFieldName) = nmRange
    lcolHash = rngHashValue(ByVal sFieldName, nmRange, oModel)
    Set oModule.dicValues(sFieldName) = lcolHash

End Sub
Public Function rngHashValue(ByVal sFieldName As String, ByRef nmRange As name, ByRef oModel As Object)
    
    Dim item As Variant
    Dim col As Collection
    Dim flds As Variant
    Dim fld As Variant
    Dim strHash As String
    Dim strVal As String
    Dim fldRange As Range
    
    
    Set fldRange = nmRange.RefersToRange
    Set col = CallByName(oModel, sFieldName, VbGet)
    flds = fldRange.CurrentRegion.rows(1)
    
    If Not IsArray(flds) Then
        ReDim flds(0)
        flds(0) = fldRange.CurrentRegion.Cells(1, 1)
        If flds(0) = "" Then
            Exit Function
        End If
    End If
    
    For Each item In col
        For Each fld In flds
            strVal = ""
            On Error Resume Next
            strVal = CStr(CallByName(item, fld, VbGet))
            On Error GoTo 0
            If Len(strVal) > 0 Then
                strHash = strHash & strVal
            End If
            
        Next fld
    Next item
    
    rngHashValue = strHash
End Function

Private Sub IModelViewLink_link(ByVal sFieldName As String, ByRef nmRange As name, ByRef oModel As Object, ByRef oModule As Object)
    Call link(sFieldName, nmRange, oModel, oModule)
End Sub

Private Function IModelViewLink_name() As String
    IModelViewLink_name = "Group"
End Function


