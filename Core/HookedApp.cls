VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "HookedApp"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
Implements IHookedApp

Private oModule As BindModule

Public dicModules As New Dictionary
Public dicLinkers As New Dictionary

Public checking As Boolean

Public Property Get modules(ByVal val As String) As Object
    Set modules = Nothing
    If dicModules.Exists(val) Then
        Set modules = dicModules(val)
    End If
End Property


Public Sub addModule(ByVal name As String, ByRef asht As Worksheet, ByRef model As Object, ParamArray dependancies() As Variant)
    
    Dim prop As String
    
    
    
    Set oModule = New BindModule
    oModule.moduleName = name
    
    Set oModule.app = Me
    Set oModule.oShtView = asht
    Set oModule.oModel = model
    
    Call setupRanges(model, asht)
    
    Set oModule.oStorage = setupStorage(dependancies(0))
    
    If UBound(dependancies) > 0 Then
        If TypeOf dependancies(0) Is IModel Then
            Call oModule.oModel.init(dependancies(0))
        End If
    End If
    
    If Not oModule.oStorage Is Nothing Then
        Call oModule.oStorage.setModule(oModule)
    End If
    
    Set dicModules(name) = oModule
    
    Call checkForModelChanges
    
End Sub

Public Sub checkForModelChanges()
    
    Dim sModuleIter As Variant
    Dim oModule As Object
    Dim changed As Boolean
    
    changed = True
    checking = True
    
    While changed
        changed = False
        For Each sModuleIter In dicModules
            Set oModule = dicModules(sModuleIter)
            changed = changed Or oModule.checkForModelChanges
        Next sModuleIter
        
    Wend
    checking = False
    
End Sub

Public Sub setupRanges(ByRef oModel As Object, ByRef oSheet As Worksheet)

    Dim nmrng As name
    Dim nmScope As String
    Dim nmField As String
    Dim nmScopeField As String
    Dim sLinker As String
    Dim oLinker As IModelViewLink

    For Each nmrng In oSheet.names
        
        nmScopeField = nmrng.name
        
        nmScope = oSheet.name
        nmField = Replace(nmScopeField, nmScope & "!", "")
        
        sLinker = "Simple"
        
        If UCase(Left(nmField, 5)) = "GROUP" Then
            sLinker = "Group"
        End If
        
        Set oLinker = dicLinkers(sLinker)
        
        Call oLinker.link(nmField, nmrng, oModel, oModule)
            
    Next nmrng


End Sub



Public Function setupModels(ParamArray objects() As Variant)

    Dim obj As Variant
    Dim oModel As IModel
    
    For Each obj In objects(0)
        On Error Resume Next
        Err.Clear
        Set oModel = obj
        
        If Err.Number = 0 Then
            Set setupModels = obj
            Exit Function
        End If
        On Error GoTo 0
    Next obj

End Function

Public Function setupStorage(ParamArray objects() As Variant)

    Dim obj As Variant
    Dim oStorage As IStorage
    
    Set setupStorage = Nothing
    
    For Each obj In objects(0)
        If TypeOf obj Is IStorage Then
            Set setupStorage = obj
            Exit Function
        
        End If
    Next obj
End Function


Public Sub init(ParamArray params() As Variant)

    Dim obj As Variant
    Dim oModelView As IModelViewLink
    
    'set up Model View Linking objects
    For Each obj In params
    
        If TypeOf obj Is IModelViewLink Then
            Set oModelView = obj
            Set dicLinkers(oModelView.name) = obj
        End If
            
    Next obj

End Sub


Private Sub IHookedApp_checkForModelChanges()
    checkForModelChanges
End Sub


Private Sub IHookedApp_init(ParamArray params() As Variant)
    Call init(params)
End Sub

Private Function IHookedApp_isChecking() As Boolean
    IHookedApp_isChecking = checking
End Function

Private Sub IHookedApp_addModule(ByVal name As String, asht As Worksheet, model As Object, ParamArray dependancies() As Variant)
    Call addModule(name, asht, model, dependancies())
End Sub

Private Property Get IHookedApp_modules(ByVal val As String) As Object
     Set IHookedApp_modules = modules(val)
End Property
