Attribute VB_Name = "XLBind"
Option Explicit
Private internalReference As Object

Public Function BindApp(Optional ByVal useInternal As Boolean = True, Optional ByVal reset = False) As IHookedApp


    If reset Then
        Set internalReference = Nothing
    End If
    
    If useInternal Then
        If internalReference Is Nothing Then
            Set internalReference = New hookedApp
            Call internalReference.init(New ModelViewLinkGroup, New ModelViewLinkSimple)
        End If
        Set BindApp = internalReference
        
        Exit Function
    End If
    
    Set BindApp = New hookedApp
    Call BindApp.init(New ModelViewLinkGroup, New ModelViewLinkSimple)
        
End Function


Public Function defaultStorage()
    Set defaultStorage = New defaultStorage
End Function
Public Function JSONStorage()
    Set JSONStorage = New JSONStorage
End Function
Public Function DefaultButton()
    Set DefaultButton = New DefaultButton
End Function



