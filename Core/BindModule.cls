VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "BindModule"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Implements IBindModule

Public WithEvents oShtView As Worksheet
Public app As IHookedApp
Public oModel As Object
Attribute oModel.VB_VarHelpID = -1
Public oStorage As IStorage

Public dicSetters As New Dictionary
Public dicGetters As New Dictionary
Public dicValues As New Dictionary
Public fields As Variant

Public moduleName As String


Public Function getGroupRange() As Range
    
    Dim oName As Variant
    Dim oRange As Range
    Dim scope As String
    Dim field As String
    Dim data As Range
    
    For Each oName In dicGetters.Items
        Set oRange = oName.RefersToRange
        scope = oRange.Parent.name
        field = UCase(Replace(oRange.name, scope & "!", ""))
        If Left(field, 5) <> "GROUP" Then
            Set data = oRange
            Exit For
        End If
        field = ""
    Next oName
    
    If field = "" Then
        Err.Raise "Should have been a " & field & " range"
    End If
    
    Set getGroupRange = data
    
End Function

Public Sub toRange()

End Sub

Public Sub fromRange()

    Dim header As Variant
    Dim column As Variant
    Dim rowCount As Long
    Dim colcount As Long
    Dim rows As Long
    Dim data As Range
    Dim item As Object
    Dim vData As Variant
    Dim fieldValue As String
    
    Dim colModelImpl As IColModel
    Set colModelImpl = oModel
    
    
    Set data = getGroupRange
    header = data.CurrentRegion.rows(1)
    On Error Resume Next
    
    Set data = data.CurrentRegion.Offset(1)
    If Err.Number <> 0 Then
        Exit Sub
    End If
    
    Set data = data.Resize(data.rows.Count - 1)
    
    rows = data.rows.Count
    vData = data
    For rowCount = 1 To rows
        Set item = colModelImpl.getClonableItem.Clone()
        colcount = 1
        For Each column In header
            fieldValue = vData(rowCount, colcount)
            CallByName item, column, VbLet, fieldValue
            colcount = colcount + 1
        Next column
        Call colModelImpl.add(item)
    Next rowCount
    
End Sub




Public Sub load()

    Dim diskStore As defaultStorage
    
    If oStorage Is Nothing Then
        
        Set diskStore = defaultStorage
        Call diskStore.init(Nothing, , ActiveWorkbook.Path & "\" & moduleName & ".txt")
        Set oStorage = diskStore
        Call oStorage.setModule(Me)
    End If

    
    oStorage.load
    app.checkForModelChanges

End Sub

Public Sub save()
    
    Dim oDefaultStore As defaultStorage
    
    If oStorage Is Nothing Then
        
        Set oDefaultStore = defaultStorage
        Call oDefaultStore.init(Nothing, , ActiveWorkbook.Path & "\" & moduleName & ".txt")
        Set oStorage = oDefaultStore
        Call oStorage.setModule(Me)
        
    End If
    
    Call oStorage.save
    
End Sub



Public Function checkForModelChanges() As Boolean
    
    Dim itm As Variant
    Dim modelval As Variant
    Dim col As Collection
    Dim viewval As Variant
    Dim sLinker As String
    
    For Each itm In dicValues
    
        On Error Resume Next
        modelval = CallByName(oModel, itm, VbGet)
        
        If Err.Number <> 0 Then
            
            Set col = CallByName(oModel, itm, VbGet)
            'TODO : put this back
            'modelval = app.collectionHash(oModel, itm, oShtView.Range("Group" & itm))
            Stop
            viewval = dicValues(itm)
            
            If CStr(modelval) <> CStr(viewval) Then
                Call displayCollection(oShtView.Range("Group" & itm), col)
                dicValues(itm) = modelval
                checkForModelChanges = True
            End If
        
        Else
            
            viewval = oShtView.Range(itm).Value2
            
            If CStr(modelval) <> CStr(viewval) Then
                oShtView.Range(itm).Value2 = modelval
                checkForModelChanges = True
            End If
            
        End If
        
        On Error GoTo 0
        
    Next itm
    
End Function

Public Sub displayCollection(ByRef rng As Range, ByRef col As Collection)
    
    Dim fld As Variant
    Dim itm As Variant
    Dim vData As Variant
    Dim lColumn As Long
    Dim lRow As Long
    
    fields = rng.CurrentRegion.rows(1)
    
    If Not IsArray(fields) Then
        ReDim fields(0)
        fields(0) = rng.CurrentRegion.Cells(1, 1).Value2
    
        If fields(0) = "" Then
            Exit Sub
        End If
    End If
    
    
    Application.EnableEvents = False
    rng.CurrentRegion.Offset(1).ClearContents
    
    ReDim vData(col.Count - 1, rng.CurrentRegion.rows(1).Columns.Count)
    
    lColumn = 0
    For Each fld In fields
        vData(0, lColumn) = fld
        lColumn = lColumn + 1
    Next fld
    
    lRow = 0
    For Each itm In col
        lColumn = 0
        For Each fld In fields
        
            'TODO: only report real fields
        
            On Error Resume Next
            vData(lRow, lColumn) = CallByName(itm, fld, VbGet)
            On Error GoTo 0
            lColumn = lColumn + 1
            
        Next fld
        lRow = lRow + 1
    Next itm
    
    rng.Resize(lRow, lColumn).Offset(1).Value2 = vData
    Application.EnableEvents = True
    
End Sub

Public Function checkItemChange() As Boolean

End Function


Public Function checkCollectionChange() As Boolean
    
End Function

Private Sub oShtView_Change(ByVal Target As Range)
    
    Dim anmRng As name
    Dim anmArea As name
    
    Dim fldName As String
    
    Set anmRng = matchedRange(Target)
    
    If Not anmRng Is Nothing Then
        fldName = Replace(anmRng.name, anmRng.Parent.name & "!", "")
        Call itemChange(anmRng, fldName, Target)
        Exit Sub
    End If
    
    Set anmArea = matchedArea(Target)
    
    If Not anmArea Is Nothing Then
        fldName = Replace(Replace(anmArea.name, anmArea.Parent.name & "!", ""), "Group", "")
        Call collChange(anmArea, fldName, Target)
        Exit Sub
    End If
    
    
    
End Sub

Public Sub collChange(ByRef anmArea As name, ByVal fldName As String, ByRef Target As Range)
    
    'found an area that matches a collection 'GroupXXX
                    
    Dim itemField As String
    Dim elementId As Long
    Dim colHash As String
    Dim col As Collection
    Dim item As Variant
    
    
    elementId = Target.Row - anmArea.RefersToRange.Row
    itemField = anmArea.RefersToRange.Cells(1, 1 + Target.column - anmArea.RefersToRange.column).Value2
    Set col = CallByName(oModel, fldName, VbGet)
    
    If elementId = 0 Then
        Application.EnableEvents = False
        anmArea.RefersToRange.CurrentRegion.Offset(1).ClearContents
        Call displayCollection(anmArea.RefersToRange, col)
        Application.EnableEvents = True
        'only change the headers
    End If
    If elementId > 0 Then
        'change actual values
        Set item = col(elementId)
        'TODO handle invalid titles
        On Error Resume Next
        Call CallByName(item, itemField, VbLet, Target.Value2)
        On Error GoTo 0
    End If
    
    'TODO: put this back
    'colHash = app.collectionHash(oModel, fldName, anmArea.RefersToRange)
    Stop
    dicValues(fldName) = colHash
    
    If Not app.isChecking Then
        app.checkForModelChanges
    End If
    
    
        
End Sub

Public Sub itemChange(ByRef anmRng As name, ByVal fldName As String, ByRef Target As Range)
        
        Dim modelValue As Variant
        modelValue = CallByName(oModel, fldName, VbGet)
        
        If modelValue <> Target.Value2 Then
            On Error Resume Next
            CallByName oModel, fldName, VbLet, Target.Value2
            On Error GoTo 0
            
            dicValues(fldName) = Target.Value2
            
            If Not app.isChecking Then
                app.checkForModelChanges
            End If
        End If
        

End Sub


Public Function matchedRange(ByRef Target As Range) As name
    
    Dim rng As Variant
    Dim scope As String
    Dim field As String
    
    scope = Target.Parent.name
    
    
    Set matchedRange = Nothing
    
    For Each rng In dicGetters.Items
    
        field = UCase(Replace(rng.name, scope & "!", ""))
        
        If Left(field, 5) <> "GROUP" Then
            If rng.RefersToRange.Parent.name & "!" & rng.RefersToRange.AddressLocal = _
                    Target.Parent.name & "!" & Target.AddressLocal Then
                
                Set matchedRange = rng
                Exit Function
            End If
        End If
        
    Next rng
    
    
End Function


Public Function matchedArea(ByRef Target As Range) As name
    
    Dim rng As Variant
    Dim areaRng As Range
    Dim scope As String
    Dim field As String
    
    Set matchedArea = Nothing
    
    Set areaRng = Target.CurrentRegion.Cells(1, 1)
    scope = areaRng.Parent.name
    
    For Each rng In dicGetters.Items
    
        field = UCase(Replace(rng.name, scope & "!", ""))
        
        If Left(field, 5) = "GROUP" Then
            If rng.RefersToRange.Parent.name & "!" & rng.RefersToRange.AddressLocal = _
                    areaRng.Parent.name & "!" & areaRng.AddressLocal Then
                
                Set matchedArea = rng
                Exit Function
            End If
        End If
        
    Next rng
    
    
End Function
