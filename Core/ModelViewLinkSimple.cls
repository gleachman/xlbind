VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "ModelViewLinkSimple"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
Implements IModelViewLink



Public Sub link(ByVal sField As String, ByRef nmRange As name, ByRef oModel As Object, ByRef oModule As Object)
    
    Dim startValue As Variant
       
    On Error Resume Next
    startValue = CallByName(oModel, sField, VbGet)
    
    If Err.Number = 0 Then
        Set oModule.dicGetters(sField) = nmRange
        oModule.dicValues(sField) = startValue
    End If
    
    Err.Clear
    
    CallByName oModel, sField, VbLet, nmRange.RefersToRange.Value2
    
    If Err.Number = 0 Then
        Set oModule.dicSetters(sField) = nmRange
    End If
    
    On Error GoTo 0
                
End Sub

Private Function IModelViewLink_name() As String
    IModelViewLink_name = "Simple"
End Function

Private Sub IModelViewLink_link(ByVal sFieldName As String, ByRef nmRange As name, ByRef oModel As Object, ByRef oModule As Object)
    Call link(sFieldName, nmRange, oModel, oModule)
End Sub


