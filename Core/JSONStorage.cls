VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "JSONStorage"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Implements IStorage
    
Public oModelStore As New Dictionary
Public oModule As Object
Public oStorageModel As Object
Public ofilePathMethod As String
Public ofilepath As String

Public Sub init(ByRef storageModel As Variant, Optional ByVal filePathMethod As String = "", Optional ByVal filepath As String = "")
    
    Set oStorageModel = storageModel
    ofilepath = filepath
    ofilePathMethod = "filepath"
    If filePathMethod <> "" Then
        ofilePathMethod = filePathMethod
    End If
   
End Sub

' This should be able to save and retrieve an entire systems data from one file.

Private Function IStorage_load() As Variant

    

End Function

Private Sub IStorage_save()

    

End Sub

Private Sub IStorage_setModule(val As Object)

End Sub
