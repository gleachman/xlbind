VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "DefaultStorage"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Implements IStorage

Public oModelStore As New Dictionary
Public oModule As Object
Public oStorageModel As Object
Public ofilePathMethod As String
Public ofilepath As String

Public Sub init(ByRef storageModel As Variant, Optional ByVal filePathMethod As String = "", Optional ByVal filepath As String = "")
    
    Set oStorageModel = storageModel
    ofilepath = filepath
    ofilePathMethod = "filepath"
    If filePathMethod <> "" Then
        ofilePathMethod = filePathMethod
    End If
   
End Sub



Private Function IStorage_load()
    
    Dim ofs As New FileSystemObject
    Dim txt As String
    Dim itm As Variant
    Dim lobj As Object
    Dim clonable As IColModel
    
    On Error Resume Next
    txt = ofs.OpenTextFile(filepath, ForReading, True).ReadAll
    On Error GoTo 0
    
    'TODO: simple text based storage for now
    '      can change later.
    
    ' an item will live on one line.
    
    ' a collection will live one item per line
    
    If InStr(txt, vbCr) Then
        ' is a collection
    
        Set clonable = oModule.oModel
        
        For Each itm In Split(txt, vbCr)
            Set lobj = clonable.getClonableItem.Clone()
            Call populateItem(itm, lobj)
            Call clonable.add(lobj)
        Next itm
        
        
    Else
        ' is a simple object
        Call populateItem(txt, oModule.oModel)
    End If
    
    
    
    
    
End Function
Public Sub populateItem(ByVal txt As String, ByVal obj As Object)
    
    Dim itm As Variant
    Dim vals As Variant
    Dim val As String
    Dim key As String
    
    For Each itm In Split(txt, ",")
        
        vals = Split(itm, ":")
        If UBound(vals) < 1 Then
            Exit For
        End If
        val = vals(1)
        key = Replace(vals(0), vbLf, "")
        On Error Resume Next
        Call CallByName(obj, key, VbLet, val)
        If Err.Number = 0 Then
            oModule.dicValues(key) = val
        End If
        On Error GoTo 0
        
    Next itm

    

End Sub
Private Sub IStorage_save()
    
    Dim itm As Variant
    Dim colItem As Variant
    Dim ofs As New FileSystemObject
    Dim fs As TextStream
    Dim txt As String
    Dim ln As String
    Dim fieldValue As String
    Dim val As Variant
    Dim headers As Variant
    Dim header As Variant
    
    For Each itm In oModule.dicGetters
        If oModule.dicSetters.Exists(itm) Then
        
            'TODO : need to distinguish collection from value
            
            
            On Error Resume Next
            val = CallByName(oModule.oModel, itm, VbGet)
            
            If Err.Number = 0 Then
                txt = txt & itm & ":" & val & ","
            Else
            
                Set val = CallByName(oModule.oModel, itm, VbGet)
                'TODO : at the moment, only store what is visible on the sheet.
                
                Set rng = oModule.getGroupRange
                headers = rng.CurrentRegion.rows(1)
                
                For Each colItem In val
                    ln = ""
                    For Each header In headers
                
                        fieldValue = CallByName(colItem, header, VbGet)
                        ln = ln & header & ":" & fieldValue & ","
                                        
                    Next header
                    
                    txt = txt & ln & vbCr
                Next colItem
                
            End If
            
        End If
    Next itm
    
    
    txt = txt & "timestamp:" & DateTime.Now
    
    Call ofs.OpenTextFile(filepath, ForWriting, True).Write(txt)
    
End Sub

Public Function filepath() As String


    If oStorageModel Is Nothing Then
        filepath = ofilepath
    Else
        filepath = CallByName(oStorageModel, ofilePathMethod, VbGet)
    End If
    
End Function

Private Sub IStorage_setModule(val As Object)
    Set oModule = val
End Sub
