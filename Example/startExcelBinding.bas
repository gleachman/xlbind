Attribute VB_Name = "startExcelBinding"
Option Explicit

''' function with model name
'       returns a worksheet
'       can have multiple model names using same sheet
'''
'   function with model name
'       return a new instance of model class

Private started As Boolean
Public Sub HookUpOnStartup()

    If started Then Exit Sub
    
    Call bindapp(, True).addModule("BaseUser", Sheet1, New User)
    
    started = True
    
End Sub

Sub btnSave()
    HookUpOnStartup
    bindapp.Modules("BaseUser").Save
End Sub

Sub btnLoad()
    HookUpOnStartup
    bindapp.Modules("BaseUser").Load
End Sub



