VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "User"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
Implements IModel

Private m_sname As String
Private m_sphone As String
Private m_spostCode As String
Private m_soutput As String

Public Property Get name() As String
    name = m_sname

End Property

Public Property Let name(ByVal sname As String)

    m_sname = sname

End Property

Public Property Get phone() As String

    phone = m_sphone

End Property

Public Property Let phone(ByVal sphone As String)

    m_sphone = sphone

End Property

Public Property Get postCode() As String

    postCode = m_spostCode

End Property

Public Property Let postCode(ByVal spostCode As String)

    m_spostCode = spostCode

End Property


Public Property Get output() As String

    output = "{ name " & name & ", phone " & phone & ", postCode " & postCode & " }"

End Property

