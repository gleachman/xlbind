VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "cSUTHookedAppSimple"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'SUT Template Start
Implements ITestAble
Option Explicit

Public t As ITestRunner
' Private WithEvents sut As sutClass
Private sut As IHookedApp

Private Function ITestAble_className() As String
    ITestAble_className = "sutHookedApp"
End Function

Private Sub ITestAble_setTestRunner(testRunner As unittestFramework.ITestRunner)
    Set t = testRunner
End Sub

Private Sub ITestAble_setUp()
    Set sut = XLBind.BindApp(, True)
    
    Dim amodel As IModel
    Set amodel = New sutHookedAppBasicModel
    
    Call sut.addModule("first", Sheet1, amodel)
    
End Sub

Private Sub ITestAble_tearDown()
End Sub


'SUT Template End

Public Sub test_WhenUsingCreateModule()
    
    
    Dim amodule As IBindModule
    Set amodule = sut.Modules("first")
    
    Call t.AssertNotNothing(amodule, "a module should have be retrievable")
    
    
End Sub

Public Sub test_HasLinkToSheet()
    
    
    Dim amodule As IBindModule
    Set amodule = sut.Modules("first")
    Dim oBindModule As BindModule
    Set oBindModule = amodule
    
    Call t.AssertEqual(oBindModule.oShtView.Name, Sheet1.Name, "a module should link to the correct sheet")
    
    
End Sub


Public Sub test_HasApp()

    
    Dim amodule As IBindModule
    Set amodule = sut.Modules("first")
    Dim oBindModule As BindModule
    Set oBindModule = amodule
    
    Call t.AssertTrue(TypeOf oBindModule.app Is IHookedApp, "The Module.App should reference hooked app")
    
    
End Sub
